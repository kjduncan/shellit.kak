Shellit.kak a [Kakoune](https://kakoune.org/) editor command line gateway.  
`/bin/sh -c $@`  
  
Shellit.kak `continuum` allows you to manage semantic versioning and changelog generation files.  
  
HTML format:  
-  [docs/javadoc/index.html](https://bitbucket.org/KJ_Duncan/shellit.kak/src/master/docs/javadoc/shellit.kak/) docstrings with UML diagrams and Harvard referencing style.  
-  [docs/tests/index.html](https://bitbucket.org/KJ_Duncan/shellit.kak/src/master/docs/tests/test/) viewable results of all tests 156/156 passing.  
-  [docs/coverage/index.html](https://bitbucket.org/KJ_Duncan/shellit.kak/src/master/docs/coverage/) 80% coverage.  
  
```shell
# If you need checksums on the jar use:
$ ./gradlew publish
$ ls -Al build/repository/org/kjd/shellit/kak/shellit.kak/3.1/
> [...]
> shellit.kak-3.1-uber.jar
> shellit.kak-3.1-uber.jar.sha256
> [...]
  
$ openssl dgst -sha256 shellit.kak-3.1-uber.jar
> SHA256(shellit.kak-3.1-uber.jar)= 5826f14d96fcf01bdb83a1c645c4415df5a4245941ff8fa871713f3a83591011
```
`build.gradle.kts` makes use of the [Maven Publish Plugin](https://docs.gradle.org/current/userguide/publishing_maven.html#header) to publish artifacts locally.  


----
 

[Usage.kt](https://bitbucket.org/KJ_Duncan/shellit.kak/src/master/src/main/kotlin/org/kjd/shellit/kak/Usage.kt) `shellit.kak [--h|-h] [continuum -h]`  


----


Hand me my man-pants, press play on the motivational song [spotify Schubert: Ave Maria, D.839](https://open.spotify.com/track/3eLlW0GegkXy3o92t51xaJ?si=9UamFauqR5iYznhZpG4NHw) or [youtube](https://youtu.be/tDQj7j-xogM) and put yourself in scrutineers mode. Peer review time!  
   
![](src/main/resources/img/InterfaceComposition.png)  
 

---


#### Building ####


@see [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html)  
  
```
# Run the gradle command from the terminal in shellit.kak top-level directory
$ gradle wrapper --gradle-version 6.8.3 --distribution-type all
  
# Linux/OSX
$ ./gradlew uberJar 
  
# Windows
$ gradlew.bat uberJar
  
# The uberJar is located
$ shellit.kak/build/libs/shellit.kak-<version>-uber.jar
  
# Build with passing *tests* use
$ ./gradlew build
  
# Shutdown the gradle daemon
$ ./gradlew --stop
  
# Gradle is a command line tool
$ ./gradlew --help
$ ./gradlew tasks --all
$ ./gradlew dependencies --configuration compileClasspath
```


---


NOTE:  
Gradle can also build shared and static libraries, and executables in [Assembler, C/C++ and Obj-C/C++](https://docs.gradle.org/current/userguide/building_cpp_projects.html)  

---


#### Installing


The Software Development Kit Manager: *'[SDKMAN](https://sdkman.io/)! installs smoothly on Mac OSX, Linux, WLS, Cygwin, Solaris and FreeBSD. We also support Bash and ZSH shells'.*  
  
```shell
$ curl -s "https://get.sdkman.io" | bash
$ source "$HOME/.sdkman/bin/sdkman-init.sh"
$ sdk version
$ sdk install java 15.0.2.hs-adpt  # moving target^1
$ sdk install kotlin
$ sdk install gradle  
```

NOTE:  
-  The package `build.gradle.kts` targets Java 15 [ProcessHandle](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/lang/ProcessHandle.Info.html)  
   -  [**1^**] Run `$ sdk list java` to see all available java sdk's.  
   -  See [sdkman](https://sdkman.io/) for more details.  
-  Hyperskill & Jetbrains Academy 2019, Theory: Java Archive, What is Java Archive?, viewed on 05 Dec 2019, https://hyperskill.org/learn/step/4311  


```shell
$ echo 'java -cp $CLASSPATH org.kjd.shellit.kak.ShellitKt "${@}"' > shellit
$ chmod u+x,u-w,og-rwx shellit
$ mv shellit $HOME/local/bin

# kakoune command
define-command -docstring "shellit /bin/sh -c $@" \
  -params .. shellit %{ nop %sh{ shellit $kak_session "${@}" } }

# or
define-command -docstring "shellit /bin/sh -c $@" \
  -params .. shellit %{ nop %sh{ java -cp $CLASSPATH org.kjd.shellit.kak.ShellitKt $kak_session "${@}" } }

alias global sh shellit

define-command -docstring 'continuum major|minor|patch "a short description"' \
  -params .. continuum %{ nop %sh{ shellit "cm" "add" "${@:1}" } }

# kakoune usage 
:sh shellit -h
:continuum patch "a workaround for positional arguments"

# expands to a POSIX shell
$ /bin/sh shellit [-h|--h|-i|-d][continuum -h] [option] [...]

---

# alternate setup for $CLASSPATH jars 
# .<shell>rc command
if [[ -d "$HOME/local/jars" ]] ; then
    JARPATH="$HOME/local/jars"
    export JARPATH
fi

javajar.sh
#!/bin/sh
# `.jar` suffix is no longer required from command line
exec java -jar "$JARPATH/$1.jar" "${@:2}"
exit 1
```

---


#### Alternate Setup (linux/osx/*nix)


```shell
# kakoune command
define-command -docstring "shellit /bin/sh -c $@" \
  -params .. shellit %{ nop %sh{ shellit.run $kak_session "${@}" } }

alias global sh shellit

# kakoune usage 
:sh shellit.run -h

---

jars-linux.sh
#!/bin/sh

##
# Tommaso, P 2019, How to make a JAR file Linux executable, corderwall.com,
# viewed on 6 Jan 2019, <https://coderwall.com/p/ssuaxa/how-to-make-a-jar-file-linux-executable>
##

MYSELF=`which "$0" 2>/dev/null`

[ $? -gt 0 -a -f "$0" ] && MYSELF="./$0"

java=java

if test -n "$JAVA_HOME"; then
    java="$JAVA_HOME/bin/java"
fi

exec "$java" $java_args -jar $MYSELF "$@"

exit 1


$ cat jars-linux.sh shellit.kak-<version>.jar > shellit.run && chmod u+x,u-w,og-rwx shellit.run && mv shellit.run $HOME/local/bin/
```
  
![](src/main/resources/img/shellit.kak-3.0-hyperfine.png)  
![](src/main/resources/img/shellit.kak-3.0-file.png)  
  
@authors preferred use-case is via [GRAALVM.md](https://bitbucket.org/KJ_Duncan/shellit.kak/src/master/GRAALVM.md) review for setup details.  


---

TODO:  

```java
/*
 * bitbucket repo  ✅
 * uml diagrams  ✅
 * logger  ✅
 * mockk   ✅
 * tests and coverage  ✅
 * docstrings and references  ✅
 * build fatjar  ✅
 * post on discuss.kakoune  ✅
 * continuum  ✅
 * dokka api 
 * workshop
 * workshop-merge
 * security.md
 */
```


---


NOTE:  
Before running `./gradlew` command run: `$ jcmd -l` or `$ pgrep java`, note the process_id of all current running java processes. Then run all the required `./gradlew` commands, after re-run the previous `jcmd` or `pgrep` command for any additional unclosed java process noting their process_id. You can then use the: `$ kill process_id` to terminate the additional running java process. The cause is strictly `./gradlew` command and *NOT* `shellit.run` or `shellit.jar`.  
@see [The Gradle Daemon](https://docs.gradle.org/current/userguide/gradle_daemon.html#gradle_daemon) for more information.  


---


For best practices with `internet-of-things::IO()`.  

```shell
# kakoune command
define-command -docstring "shellit /bin/sh -c $@" \ 
  -params .. shellit %{ nop %sh{ run-shellit-kak.sh $kak_session "${@}" } }

alias global sh shellit


# run-shellit-kak.sh
#!/bin/sh
java -Djava.security.manager \
     -Djava.security.policy==<your>.policy \
     -cp <path>/shellit.kak-<version>-uber.jar \
     org.kjd.shellit.kak.ShellitKt "${@}"  
```


---


##### Kakoune General Information


Work done? Have some fun. Share any improvements, ideas or thoughts with the community [discuss.kakoune](https://discuss.kakoune.com/).  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _’community plugins’_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 180+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  



---


##### IntelliJ IDEA by Jetbrains


[Jetbrains](https://www.jetbrains.com/) thank you:  

-  [IntelliJ](https://www.jetbrains.com/idea/)
-  [Kotlin](https://kotlinlang.org/)
-  [hi.hyperskill](https://hi.hyperskill.org/)
-  [University subscription scheme](https://www.jetbrains.com/community/education/#students)


A team effort well done.  
  
Kakoune [friends](https://github.com/mawww/kakoune/graphs/contributors) consider [Licences for Open Source](https://www.jetbrains.com/community/opensource/#support) and get some [C/C++](https://www.jetbrains.com/cpp/) with [external tool](https://www.jetbrains.com/help/clion/configuring-third-party-tools.html) integration.   
  
Captain Kirk on the Starship [ICT352](https://www.usc.edu.au/study/courses-and-programs/courses/course-library/ict/ict352-project-management) in [Space](https://www.jetbrains.com/space/) the final ~~frontier~~ year!   


---


That's it for the readme, anything else you may need to know just pick up a book and read it [_Polar Bookshelf_](https://getpolarized.io/). Thanks all. Bye.  

