#### As at 09/04/2020 ####


```shell
$ sdk install java 20.0.0.r11-grl  # moving target

$ $GRAALVM_HOME/bin/gu install -h
$ $GRAALVM_HOME/bin/gu available
$ $GRAALVM_HOME/bin/gu install native-image


$ $GRAALVM_HOME/bin/native-image -cp ./shellit.kak-<version>.jar -O1 -H:Name=shellit.kak -H:Class=org.kjd.shellit.kak.ShellitKt -H:+ReportExceptionStackTraces --no-server --no-fallback
[...building...]
$ ls
shellit.kak
$ chmod u-w,og-rwx shellit.kak
$ ls -l shellit.kak
-r-x------ 1 user group 12M date time shellit.kak
$ mv shellit.kak $HOME/local/bin


# kakoune command
define-command -docstring "shellit /bin/sh -c $@" \
  -params .. shellit %{ nop %sh{ shellit.kak $kak_session "${@}" } }

alias global sh shellit

# kakoune usage 
:sh shellit.kak -h
```

#### GraalVM Native Image Reference ####

Christian Wimmer, Codrut Stancu, Peter Hofer, Vojin Jovanovic, Paul Wögerer, Peter B. Kessler, Oleg Pliss,and Thomas Würthinger. 2019. **Initialize Once, Start Fast: Application Initialization at Build Time**. Proc. ACM Program. Lang.3, OOPSLA, Article 184 (October 2019), 29 pages. https://doi.org/10.1145/3360610


#### GraalVM Native Image Manual ####

[graalvm native image documentation](https://www.graalvm.org/docs/reference-manual/native-image/)

