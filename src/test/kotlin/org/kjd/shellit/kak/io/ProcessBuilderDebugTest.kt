package org.kjd.shellit.kak.io

import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.kjd.shellit.kak.client.CliArgs
import org.kjd.shellit.kak.client.CommandLine
import org.kjd.shellit.kak.client.KakClient
import org.kjd.shellit.kak.client.Message
import java.io.File

internal class ProcessBuilderDebugTest {

  private object ObjProcessBuilderDebugImpl :
    ProcessBuilderDebugImpl<ObjProcessBuilderDebugImpl> {}

  private val fileMock: Lazy<File> = mockk(relaxed = true)

  private fun getCommandLine(
    kakClient: KakClient,
    cliArgs: CliArgs,
    message: Message,
    lazyFile: Lazy<File> = lazy {
      createTempFile("kak-shellit-", "testing")
    }.also { it.value.deleteOnExit() }
  ): CommandLine =
    CommandLine(kakClient, cliArgs, message, lazyFile)

  @Test
  fun `ProcessBuilderDebugImpl is not null`() {
    val commandLine: CommandLine = getCommandLine(
      kakClient = KakClient("generic"),
      cliArgs = CliArgs(listOf("ls -al")),
      message = Message("received")
    )

    ObjProcessBuilderDebugImpl.run {
      assertNotNull(processBuilderOnDebug(commandLine))
    }
  }

  @Test
  fun `processBuilderOnDebug does not throw Exception on 'pwd'`() {
    val commandLine: CommandLine = getCommandLine(
      kakClient = KakClient("generic"),
      cliArgs = CliArgs(listOf("pwd")),
      message = Message("received")
    )

    ObjProcessBuilderDebugImpl.run {
      assertDoesNotThrow {
        processBuilderOnDebug(commandLine)
          .runReader(commandLine)
      }
    }
  }

  // ("throwing(java.io.IOException: Cannot run program \"poq\": error=2, No such file or directory)")
  @Test
  fun `processBuilderOnDebug does not throw Exception and system exit without the runReader context call`() {
    val commandLine: CommandLine =
      CommandLine(
        kakClient = KakClient("generic"),
        cliArgs = CliArgs(listOf("bad command")),
        message = Message("error")
      )

    ObjProcessBuilderDebugImpl.run {
      assertDoesNotThrow {
        processBuilderOnDebug(commandLine)
      }
    }
  }

  @Test
  fun `processBuilderOnDebug should be of instance Reader(CommandLine, Unit) prior to runReader call and Unit after the call`() {
    val commandLine: CommandLine = getCommandLine(
      kakClient = KakClient("kak"),
      cliArgs = CliArgs(listOf("pwd")),
      message = Message("reader closure")
    )

    ObjProcessBuilderDebugImpl.run {
      val reader = processBuilderOnDebug(commandLine)
      assertThat(reader).isSameAs(reader)
      assertSame(Unit, reader.runReader(commandLine))
    }
  }

  @Test
  fun `processBuilderDebugCommands returns a modified process builder`() {
    ObjProcessBuilderDebugImpl.run {
      val pb: ProcessBuilder = processBuilderDebugCommands(listOf("pwd"), ProcessBuilder())
      assertThat(pb.command()).isNotSameAs(processBuilderDebugCommands(listOf("ls -l"), pb).command())
    }
  }
}
