package org.kjd.shellit.kak.io

import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.kjd.shellit.kak.client.CliArgs
import org.kjd.shellit.kak.client.CommandLine
import org.kjd.shellit.kak.client.Commands
import org.kjd.shellit.kak.client.KakClient
import org.kjd.shellit.kak.client.Message

internal class ProcessBuilderShellTest {

  private object ObjProcessBuilderShellImpl :
    ProcessBuilderShellImpl<ObjProcessBuilderShellImpl> {}

  private val commands: List<String> = listOf<String>("pwd")

  private val pb: ProcessBuilder = mockk(relaxed = true)

  private val commandLineMock: CommandLine = mockk(relaxed = true)

  private val commandLine = CommandLine(
    kakClient = KakClient("knowledgeable-kettle"),
    cliArgs = CliArgs(commands),
    message = Message("ShellitKt"),
    lazyFile = lazy {
      createTempFile("kak-shellit-", "testing")
    }.also { it.value.deleteOnExit() }
  )

  @BeforeAll
  fun `mock ProcessBuilderShellImpl object and processBuilderCommands method before all tests`() {
    mockkObject(ObjProcessBuilderShellImpl)

    every {
      ObjProcessBuilderShellImpl.run {
        processBuilderCommands(
          commands,
          pb
        )
      }
    } returns pb
  }

  @BeforeAll
  fun `mock data class commandLine properties before all tests`() {
    val kakMessageMarkupListOfStrings: List<String> = listOf<String>(
      "/bin/sh",
      "-c",
      "echo \"hook -always -once global NormalIdle .* %{ markup }\" | kak -p kakClient"
    )

    val kakConnectProcessListOfStrings: List<String> =
      listOf<String>("/bin/sh", "-c", "echo 'edit! -existing file' | kak -p kakClient")

    val lazyKakMessageMarkupMock: Lazy<Commands> = mockk()
    val lazyKakConnectProcessMock: Lazy<Commands> = mockk()

    every { commandLineMock.kakMessageMarkup } returns lazyKakMessageMarkupMock
    every { commandLineMock.kakConnectProcess } returns lazyKakConnectProcessMock

    every { commandLineMock.kakMessageMarkup.value.cmds } returns kakMessageMarkupListOfStrings
    every { commandLineMock.kakConnectProcess.value.cmds } returns kakConnectProcessListOfStrings
  }

  @AfterAll
  fun `unmock all objects after tests`() {
    unmockkAll()
  }

  @Nested
  inner class ProcessBuilderCommandsTest {

    @Test
    fun `should not be null`() {
      ObjProcessBuilderShellImpl.run {
        assertNotNull(processBuilderCommands(commands, pb))
      }
    }

    @Test
    fun `returns the same processBuilder object`() {
      ObjProcessBuilderShellImpl.run {
        assertSame(pb, processBuilderCommands(commands, pb))
      }
    }
  }

  @Nested
  inner class ProcessBuilderConnectToKakProcessTest {

    @Test
    fun `is not null`() {
      ObjProcessBuilderShellImpl.run {
        assertNotNull(processBuilderConnectToKakProcess(commandLineMock))
      }
    }

    @Test
    fun `returns true`() {
      ObjProcessBuilderShellImpl.run {
        assertTrue(
          processBuilderConnectToKakProcess(commandLineMock)
            .runReader(commandLineMock)
        )
      }
    }
  }

  @Nested
  inner class ProcessBuilderRedirectOutputTest {

    @Test
    fun `is not null`() {
      ObjProcessBuilderShellImpl.run {
        assertNotNull(processBuilderRedirectOutput(commandLineMock))
      }
    }

    @Test
    fun `returns the same objects commandLine and processBuilderRedirectOutput`() {
      ObjProcessBuilderShellImpl.run {
        assertSame(commandLine, processBuilderRedirectOutput(commandLine).runReader(this))
      }
    }

    @Test
    fun `commandLine is equal to processBuilderRedirectOutput(commandLine) runReader(this)`() {
      ObjProcessBuilderShellImpl.run {
        assertEquals(commandLine, processBuilderRedirectOutput(commandLine).runReader(this))
      }
    }
  }

  @Nested
  inner class ProcessBuilderIgnoreOutputTest {

    @Test
    fun `is not null`() {
      ObjProcessBuilderShellImpl.run {
        assertNotNull(processBuilderIgnoreOutput(commandLineMock))
      }
    }

    @Test
    fun `same objects commandLine and processBuilderIgnoreOutput returns`() {
      ObjProcessBuilderShellImpl.run {
        assertSame(
          commandLineMock,
          processBuilderIgnoreOutput(commandLineMock)
            .runReader(ObjProcessBuilderShellImpl)
        )
      }
    }

    @Test
    fun `commandLine equals processBuilderIgnoreOutput return`() {
      ObjProcessBuilderShellImpl.run {
        assertEquals(
          commandLineMock,
          processBuilderIgnoreOutput(commandLineMock)
            .runReader(ObjProcessBuilderShellImpl)
        )
      }
    }
  }

  @Nested
  inner class ProcessBuilderOnErrorTest {

    @Test
    fun `is not null`() {
      ObjProcessBuilderShellImpl.run {
        assertNotNull(processBuilderOnError(commandLineMock))
      }
    }

    @Test
    fun `returns true`() {
      ObjProcessBuilderShellImpl.run {
        assertTrue(
          processBuilderOnError(commandLineMock)
            .runReader(commandLineMock)
        )
      }
    }

    @Test
    fun `true equals return`() {
      ObjProcessBuilderShellImpl.run {
        assertEquals(
          true, processBuilderOnError(commandLineMock)
            .runReader(commandLineMock)
        )
      }
    }
  }

  @Nested
  inner class ProcessBuilderOnSuccessTest {

    @Test
    fun `is not null`() {
      ObjProcessBuilderShellImpl.run {
        assertNotNull(processBuilderOnSuccess(commandLineMock))
      }
    }

    @Test
    fun `returns true`() {
      ObjProcessBuilderShellImpl.run {
        assertTrue(
          processBuilderOnSuccess(commandLineMock)
            .runReader(commandLineMock)
        )
      }
    }
  }
}
