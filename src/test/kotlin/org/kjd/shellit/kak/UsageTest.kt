package org.kjd.shellit.kak

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class UsageTest {

  @Test
  fun `Usage object exists`() {
    assertNotNull(Usage)
  }

  @Test
  fun `Usage const val MESSAGE is not null`() {
    assertNotNull(Usage.MESSAGE_LONG)
  }

  @Test
  fun `Usage MESSAGE should contain string *debug*`() {
    assertThat(Usage.MESSAGE_LONG).contains("*debug*")
  }
}
