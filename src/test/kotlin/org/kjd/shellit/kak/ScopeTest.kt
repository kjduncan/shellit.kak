package org.kjd.shellit.kak

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.kjd.shellit.kak.continuum.ContinuumImpl
import org.kjd.shellit.kak.io.ProcessBuilderDebugImpl
import org.kjd.shellit.kak.io.ProcessBuilderShellImpl

internal class ScopeTest {

  @Nested
  inner class ShellTest {
    private val shell: Shell = object : Shell {}

    @Test
    fun `Shell is not null and an interface`() {
      assertNotNull(Shell::class)
      assertThat(Shell::class.java).isInterface
    }

    @Test
    fun `Shell object should not be null`() {
      assertNotNull(shell)
    }

    @Test
    fun `Shell interface should be instance of Shell`() {
      assertThat(shell).isInstanceOf(Shell::class.java)
    }

    @Test
    fun `Shell interface should be instance of ParseCliArgsImpl Shell`() {
      assertThat(shell).isInstanceOf(ParseCliArgsImpl::class.java)
    }

    @Test
    fun `Shell interface should be ProcessBuilderShellImpl Shell`() {
      assertThat(shell).isInstanceOf(ProcessBuilderShellImpl::class.java)
    }
  }

  @Nested
  inner class DebugTest {
    private val debug: Debug = object : Debug {}

    @Test
    fun `Debug is not null and an interface`() {
      assertNotNull(Debug::class)
      assertThat(Debug::class.java).isInterface
    }

    @Test
    fun `Debug interface should not be null`() {
      assertNotNull(debug)
    }

    @Test
    fun `Debug interface should be instance of Debug`() {
      assertThat(debug).isInstanceOf(Debug::class.java)
    }

    @Test
    fun `Debug interface should be instance of ParseCliArgsImpl Debug`() {
      assertThat(debug).isInstanceOf(ParseCliArgsImpl::class.java)
    }

    @Test
    fun `Debug interface should be ProcessBuilderDebugImpl Debug`() {
      assertThat(debug).isInstanceOf(ProcessBuilderDebugImpl::class.java)
    }
  }

  @Nested
  inner class VariantTest {
    private val variant: Variant = object : Variant {}

    @Test
    fun `Variant is not null and an interface`() {
      assertNotNull(Variant::class)
      assertThat(Variant::class.java).isInterface
    }

    @Test
    fun `Variant interface should not be null`() {
      assertNotNull(variant)
    }

    @Test
    fun `Variant interface should be instance of Variant`() {
      assertThat(variant).isInstanceOf(Variant::class.java)
    }

    @Test
    fun `Variant interface should be instance of ParseCliArgsImpl Variant`() {
      assertThat(variant).isInstanceOf(ContinuumCliArgsImpl::class.java)
    }

    @Test
    fun `Variant interface should be instance of ContinuumImpl Variant`() {
      assertThat(variant).isInstanceOf(ContinuumImpl::class.java)
    }
  }
}
