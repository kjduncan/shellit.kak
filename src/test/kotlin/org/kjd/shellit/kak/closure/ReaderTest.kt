package org.kjd.shellit.kak.closure

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ReaderTest {

  private val reader: Reader<Int, Int> =
    Reader { it * 8 }

  @Test
  fun `Reader should exist`() {
    assertNotNull(Reader)
  }

  @Test
  fun `Reader runReader should not be null`() {
    assertNotNull(reader.runReader)
  }

  @Test
  fun `Reader(Int, Int) should be less than or equal to 16`() {
    assertThat(reader.runReader(2)).isLessThanOrEqualTo(16)
  }

  @Test
  fun `Reader(Int, Int) should be 16`() {
    assertEquals(16, reader.runReader(2))
  }

  @Test
  fun `Reader(Int, Int) map int toString should be "16"`() {
    assertEquals("16", reader.map { int -> int.toString() }.runReader(2))
  }

  @Test
  fun `Reader(Int, Int) flatMap pure(Int, String) should not be Reader(Int, Int)`() {
    assertThat(reader)
      .isNotSameAs(reader
        .flatMap { int -> Reader.pure<Int, String>("${int * int}") })
  }

  @Test
  fun `Reader(Int, Int) flatMap pure("it * it") should equal "64"`() {
    reader.flatMap { int -> Reader.pure<Int, String>("${int * int}") }
      .map { assertEquals("64", it) }
  }

  @Test
  fun `Reader(Int, Int) pure(11) flatMap should be 11^2 = 121`() {
    assertEquals("121", Reader.pure<Int, Int>(11)
      .flatMap { int -> Reader.pure<Int, String>("${int * int}") }
      .runReader(0))
  }

  @Test
  fun `Reader Companion method pure 6 to runReader property method should not be 0`() {
    assertNotEquals(0, Reader.pure<Int, Int>(6).runReader(0))
  }

  @Test
  fun `Reader Companion method pure 6 to runReader property method 0 should be 6`() {
    assertEquals(6, Reader.pure<Int, Int>(6).runReader(0))
  }

  @Test
  fun `Reader Companion method ask should be in closure kotlin Unit`() {
    assertSame(Unit, Reader.ask<Unit>().runReader(Unit))
  }

  @Test
  fun `Reader Companion method ask should be in closure kotlin String`() {
    assertSame("closure", Reader.ask<String>().runReader("closure"))
  }
}
