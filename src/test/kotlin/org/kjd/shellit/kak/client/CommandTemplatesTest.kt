package org.kjd.shellit.kak.client

import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.File

internal class CommandTemplatesTest {

  private val fileMock: File = mockk()

  @AfterAll
  fun `after all unmockk fileMock`() {
    unmockkAll()
  }

  @Test
  fun `markupInformationTemplate function should not be null`() {
    assertNotNull(markupSuccessTemplate(""))
  }

  @Test
  @DisplayName("markupSuccessTemplate(\"message\") should equal \"echo -markup '{Information}Shell onload message ALL GOOD'\"")
  fun `markupInformationTemplate should equal message string`() {
    assertTrue(
      markupSuccessTemplate("message")
        .contentEquals("${KakCli.ECHO} ${KakCli.FLAG_MARKUP} '{Information}Shell onload message ALL GOOD'")
    )
  }

  @Test
  @DisplayName("markupErrorTemplate(\"message\") should equal \"echo -markup '{Error}Shell onload message WENT BAD'\"")
  fun `markupErrorTemplate should equal message string`() {
    assertTrue(
      markupFailureTemplate("message")
        .contentEquals("${KakCli.ECHO} ${KakCli.FLAG_MARKUP} '{Error}Shell onload message WENT BAD'")
    )
  }

  @Test
  @DisplayName("markupDebugTemplate(\"message\") should equal \"echo -debug message\"")
  fun `markupDebugTemplate should equal message string`() {
    assertTrue(markupDebugTemplate("message").contentEquals("${KakCli.ECHO} ${KakCli.FLAG_DEBUG} message"))
  }

  @Test
  @DisplayName("markupMessageTemplate(KakClient(\"\"), Message(\"\")).cmds.size should equal 3")
  fun `markupMessageTemplate size should be exactly 3`() {
    assertEquals(3, markupMessageTemplate(KakClient(""), Message("")).cmds.size)
  }

  @Test
  @DisplayName("markupMessageTemplate(KakClient(\"\"), Message(\"\")) should equal Commands(listOf(\"/bin/sh\", \"-c\", \"echo \"hook always once global NormalIdle .* %{ }\" | kak -p \"))")
  fun `markupMessageTemplate should equal message string`() {
    assertEquals(markupMessageTemplate(KakClient(""), Message("")), Commands(
        listOf(
          "${ShellCli.SLASH}${ShellCli.BIN}${ShellCli.SLASH}${ShellCli.SHELL}",
          ShellCli.FLAG_C,
          "${ShellCli.ECHO} \"${KakCli.HOOK} ${KakCli.ALWAYS} ${KakCli.ONCE} ${KakCli.GLOBAL} ${KakCli.NORMAL_IDLE} .* %{  }\" ${ShellCli.PIPE} ${KakCli.KAK} ${KakCli.FLAG_P} "
        )
      )
    )
  }

  @Test
  @DisplayName("connectProcessTemplate(KakClient(\"\"), fileMock).cmds.size should equal 3")
  fun `connectProcessTemplate cmds size should be exactly 3`() {
    assertEquals(3, connectProcessTemplate(KakClient(""), fileMock).cmds.size)
  }

  @Test
  @DisplayName("connectProcessTemplate(KakClient(\"\"), fileMock) should equal Commands(listOf(\"/bin/sh\", \"-c\", \"echo 'edit -existing file' | kak -p ; echo \"hook global KakEnd .* %{ nop %sh{ rm file } }\" | kak -p ))")
  fun `connectProcessTemplate should equal message string`() {
    assertEquals(
      connectProcessTemplate(KakClient(""), fileMock), Commands(
        listOf(
          "${ShellCli.SLASH}${ShellCli.BIN}${ShellCli.SLASH}${ShellCli.SHELL}",
          ShellCli.FLAG_C,
          "${ShellCli.ECHO} '${KakCli.EDIT} ${KakCli.FLAG_EXISTING} ${fileMock}' ${ShellCli.PIPE} ${KakCli.KAK} ${KakCli.FLAG_P} ; ${ShellCli.ECHO} \"${KakCli.HOOK} ${KakCli.GLOBAL} ${KakCli.KAK_END} .* %{ ${KakCli.NOP} %sh{ ${ShellCli.RM} ${fileMock} } }\" ${ShellCli.PIPE} ${KakCli.KAK} ${KakCli.FLAG_P} "
        )
      )
    )
  }

  @Test
  @DisplayName("shellProcessTemplate listOf(\"cat afile.txt > bfile.txt\").size should be exactly 3")
  fun `shellProcessTemplate list size should be exactly 3`() {
    assertEquals(3, shellProcessTemplate(CliArgs(listOf("cat afile.txt > bfile.txt"))).cmds.size)
  }

  @Test
  @DisplayName("shellProcessTemplate listOf(\"cat\", \"afile.txt\", \">\", \"bfile.txt\").size should be exactly 3")
  fun `shellProcessTemplate listOf size should be exactly 3`() {
    assertEquals(3, shellProcessTemplate(CliArgs(listOf("cat", "afile.txt", ">", "bfile.txt"))).cmds.size)
  }

  @Test
  @DisplayName("shellProcessTemplate(CliArgs(listOf(\"cat afile.txt > bfile.txt\"))) should equal Commands(listOf(\"/bin/sh\", \"-c\", \"cat afile.txt > bfile.txt\"")
  fun `shellProcessTemplate should equal message string`() {
    assertEquals(
      shellProcessTemplate(CliArgs(listOf("cat afile.txt > bfile.txt"))), Commands(
        listOf(
          "${ShellCli.SLASH}${ShellCli.BIN}${ShellCli.SLASH}${ShellCli.SHELL}",
          ShellCli.FLAG_C,
          "cat afile.txt > bfile.txt"
        )
      )
    )
  }
}
