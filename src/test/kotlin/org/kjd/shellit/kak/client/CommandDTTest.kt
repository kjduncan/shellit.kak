package org.kjd.shellit.kak.client

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class CommandDTTest {

  private val commandLineTest: CommandLine =
    CommandLine(
      kakClient = KakClient(client = "knowledgeable-kettle"),
      cliArgs = CliArgs(args = listOf("pwd")),
      message = Message("ShellitKt")
    )

  @Nested
  inner class KakClientTest {

    @Test
    fun `kakClient property should not be null`() {
      assertNotNull(commandLineTest.kakClient)
    }

    @Test
    fun `kakClient property equals knowledgeable-kettle`() {
      assertEquals("knowledgeable-kettle", commandLineTest.kakClient.client)
    }

    @Test
    fun `kakClient property should be a String length 20`() {
      assertEquals(20, commandLineTest.kakClient.client.length)
    }

    @Test
    fun `kakClient property should contain (knowledgeable|-|kernel)`() {
      assertEquals(
        "Commands(cmds=[/bin/sh, -c, echo \"hook -always -once global NormalIdle .* %{ ShellitKt }\" | kak -p knowledgeable-kettle])",
        commandLineTest.kakMessageMarkup.value.toString()
      )
    }
  }

  @Nested
  inner class CliArgsTest {
    @Test
    fun `cliArgs property should not be null`() {
      assertNotNull(commandLineTest.cliArgs)
    }

    @Test
    fun `cliArgs property should be a List String  size 1`() {
      assertEquals(1, commandLineTest.cliArgs.args.size)
    }

    @Test
    fun `cliArgs property should contain pwd`() {
      assertThat(commandLineTest.cliArgs.args.toString()).contains("pwd")
    }
  }

  @Nested
  inner class MessageTest {
    @Test
    fun `Message property should not be null`() {
      assertNotNull(commandLineTest.message)
    }

    @Test
    fun `markup property should be a String length 8`() {
      assertEquals(9, commandLineTest.message.markup.length)
    }

    @Test
    fun `markup property should contain ShellitKt`() {
      assertThat(commandLineTest.message.markup).contains("ShellitKt")
    }
  }

  @Nested
  inner class LazyFileTest {
    @Test
    fun `lazyFile should not be null`() {
      assertNotNull(commandLineTest.lazyFile)
    }

    @Test
    fun `lazyFile should be an instance of Lazy File`() {
      assertThat(commandLineTest.lazyFile).isInstanceOf(Lazy::class.java)
    }

    @Test
    fun `lazyFile should be a file`() {
      assertTrue(commandLineTest.lazyFile.value.isFile)
    }

    @Test
    fun `lazyFile should be empty`() {
      assertTrue(commandLineTest.lazyFile.value.length() == 0L)
    }

    @Test
    fun `lazyFile should be writeable`() {
      assertTrue(commandLineTest.lazyFile.value.canWrite())
    }

    @Test
    fun `lazyFile should have extension tmp`() {
      assertTrue(commandLineTest.lazyFile.value.name.endsWith(".tmp"))
    }

    @Test
    fun `lazyFile name should contain kak-shellit`() {
      assertTrue(commandLineTest.lazyFile.value.name.contains("kak-shellit".toRegex()))
    }
  }

  @Nested
  inner class KakMessageMarkupTest {
    @Test
    fun `kakMessageMarkup property should not be null`() {
      assertNotNull(commandLineTest.kakMessageMarkup)
    }

    @Test
    fun `kakMessageMarkup value lazy function should not be null`() {
      assertNotNull(commandLineTest.kakMessageMarkup.value)
    }

    @Test
    fun `kakMessageMarkup value lazy function should be a List String  size 3`() {
      assertEquals(3, commandLineTest.kakMessageMarkup.value.cmds.size)
    }

    @Test
    fun `kakMessageMarkup value lazy function should contain (sh|kak|-c|Shellit|echo)`() {
      assertThat(commandLineTest.kakMessageMarkup.value.toString())
        .contains("sh", "kak", "-c", "Shellit", "echo")
    }
  }

  @Nested
  inner class KakConnectProcessTest {
    @Test
    fun `kakConnectProcess property should not be null`() {
      assertNotNull(commandLineTest.kakConnectProcess)
    }

    @Test
    fun `kakConnectProcess value lazy function should not be null`() {
      assertNotNull(commandLineTest.kakConnectProcess.value)
    }

    @Test
    fun `kakConnectProcess value lazy function should be a List String  size 3`() {
      assertEquals(3, commandLineTest.kakConnectProcess.value.cmds.size)
    }

    @Test
    fun `kakConnectProcess value lazy function should contain (sh|kak|-c|shellit|echo)`() {
      assertThat(commandLineTest.kakConnectProcess.value.toString())
        .contains("sh", "kak", "-c", "shellit", "echo")
    }
  }
}
