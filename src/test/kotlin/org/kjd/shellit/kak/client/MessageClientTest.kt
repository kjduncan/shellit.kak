package org.kjd.shellit.kak.client

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class MessageClientTest {

  @Test
  fun `OnSuccess should exist`() {
    assertNotNull(OnSuccess.markup("MessageClientTest"))
  }

  @Test
  fun `OnSuccess should be echo -markup Information Shell onload MessageClientTest ALL GOOD`() {
    assertTrue(
      OnSuccess.markup("MessageClientTest").toString()
        .contains("echo -markup '{Information}Shell onload MessageClientTest ALL GOOD'")
    )
  }

  @Test
  fun `OnFailure should exist`() {
    assertNotNull(OnFailure.markup("MessageClientTest"))
  }

  @Test
  fun `OnFailure should be echo -markup Error Shell onload MessageClientTest WENT BAD`() {
    assertTrue(
      OnFailure.markup("MessageClientTest").toString()
        .contains("echo -markup '{Error}Shell onload MessageClientTest WENT BAD'")
    )
  }
}
