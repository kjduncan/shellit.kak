package org.kjd.shellit.kak.client

/**
 * KakCli properties are kotlin.String constants and represent a subset
 * of available Kakoune command line parameters `kak -help` for details.
 *
 * [ExecCli UML](/shellit.kak/src/main/resources/img/ExecCli.png)
 *
 * @property KAK kak
 * @property EDIT edit!
 * @property FLAG_L -l
 * @property FLAG_C -c
 * @property FLAG_P -p
 * @property FLAG_EXISTING -existing
 * @property FLAG_MARKUP -markup
 * @property FLAG_DEBUG -debug
 * @property NOP nop
 * @property HOOK hook
 * @property GLOBAL global
 * @property ECHO echo
 * @property ALWAYS -always
 * @property ONCE -once
 * @property NORMAL_IDLE NormalIdle
 * @property KAK_END KakEnd
 */
object KakCli {
  const val KAK: String           = "kak"
  const val EDIT: String          = "edit!"
  const val FLAG_L: String        = "-l"
  const val FLAG_C: String        = "-c"
  const val FLAG_P: String        = "-p"
  const val FLAG_EXISTING: String = "-existing"
  const val FLAG_MARKUP: String   = "-markup"
  const val FLAG_DEBUG: String    = "-debug"
  const val NOP: String           = "nop"
  const val HOOK: String          = "hook"
  const val GLOBAL: String        = "global"
  const val ECHO: String          = "echo"
  const val ALWAYS: String        = "-always"
  const val ONCE: String          = "-once"
  const val NORMAL_IDLE: String   = "NormalIdle"
  const val KAK_END: String       = "KakEnd"
}

/**
 * ShellCli properties are kotlin.String constants and represent a subset
 * of available shell commands `sh --help` for details.
 *
 * [ExecCli UML](/shellit.kak/src/main/resources/img/ExecCli.png)
 *
 * @property BIN bin
 * @property SHELL sh
 * @property FLAG_C -c
 * @property ECHO echo
 * @property PIPE |
 * @property SLASH /
 * @property RM rm
 */
object ShellCli {
  const val BIN: String    = "bin"
  const val SHELL: String  = "sh"
  const val FLAG_C: String = "-c"
  const val ECHO: String   = "echo"
  const val PIPE: String   = "|"
  const val SLASH: String  = "/"
  const val RM: String     = "rm"
}
