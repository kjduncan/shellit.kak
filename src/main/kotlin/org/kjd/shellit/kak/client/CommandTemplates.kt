package org.kjd.shellit.kak.client

import java.io.File

/**
 * markupSuccessTemplate is a string command template.
 *
 * [Command Templates UML](/shellit.kak/src/main/resources/img/CommandTemplates.png)
 *
 * `echo -markup '{Information}Shell onload message ALL GOOD'`
 *
 * @param message the list of command line arguments
 * @see [org.kjd.shellit.kak.client.CliArgs]
 * @return [kotlin.String]
 */
fun markupSuccessTemplate(message: String): String =
  "${KakCli.ECHO} ${KakCli.FLAG_MARKUP} '{Information}Shell onload $message ALL GOOD'"

/**
 * markupFailureTemplate is a string command template.
 *
 * `echo -markup '{Error}Shell onload message WENT BAD'`
 *
 * @param message from exception thrown
 * @return [kotlin.String]
 */
fun markupFailureTemplate(message: String): String =
  "${KakCli.ECHO} ${KakCli.FLAG_MARKUP} '{Error}Shell onload ${message.replace("$", " ")} WENT BAD'"

/**
 * markupDebugTemplate is a string command template.
 *
 * `echo -debug message`
 *
 * @param message from exception thrown
 * @return [kotlin.String]
 */
fun markupDebugTemplate(message: String): String =
  "${KakCli.ECHO} ${KakCli.FLAG_DEBUG} ${message.replace("$", " ")}"

/**
 * markupMessageTemplate is a kotlin.collections.List template for a kakoune client command.
 *
 * `echo "hook -always -once global NormalIdle .* %{ message }" | kak -p kakClient`
 *
 * @param kakClient is [CommandLine.kakClient]
 * @see [org.kjd.shellit.kak.client.KakClient]
 *
 * @param message
 * @see [org.kjd.shellit.kak.client.Message]
 *
 * @return Commands is a type safe wrapper around [kotlin.collections.List] String
 * @see [org.kjd.shellit.kak.client.Commands]
 */
fun markupMessageTemplate(kakClient: KakClient, message: Message): Commands =
  Commands(
    listOf(
      "${ShellCli.SLASH}${ShellCli.BIN}${ShellCli.SLASH}${ShellCli.SHELL}",
      ShellCli.FLAG_C,
      "${ShellCli.ECHO} \"${KakCli.HOOK} ${KakCli.ALWAYS} ${KakCli.ONCE} ${KakCli.GLOBAL} ${KakCli.NORMAL_IDLE} .* %{ ${message.markup} }\" ${ShellCli.PIPE} ${KakCli.KAK} ${KakCli.FLAG_P} ${kakClient.client}"
    )
  )

/**
 * connectProcessTemplate is a kotlin.collections.List template
 * for a kakoune client command.
 *
 * `echo 'edit! -existing tempFile' | kak -p kakClient; echo "hook global KakEnd .* %{ nop %sh{ rm tempFile } }" | kak -p kakClient`
 *
 * @param kakClient is [CommandLine.kakClient]
 * @see [org.kjd.shellit.kak.client.KakClient]
 *
 * @param tempFile is [CommandLine.lazyFile]
 *
 * @return Commands is a type safe wrapper around [kotlin.collections.List] String
 * @see [org.kjd.shellit.kak.client.Commands]
 */
fun connectProcessTemplate(kakClient: KakClient, tempFile: File): Commands =
  Commands(
    listOf(
      "${ShellCli.SLASH}${ShellCli.BIN}${ShellCli.SLASH}${ShellCli.SHELL}",
      ShellCli.FLAG_C,
      "${ShellCli.ECHO} '${KakCli.EDIT} ${KakCli.FLAG_EXISTING} ${tempFile}' ${ShellCli.PIPE} ${KakCli.KAK} ${KakCli.FLAG_P} ${kakClient.client}; ${ShellCli.ECHO} \"${KakCli.HOOK} ${KakCli.GLOBAL} ${KakCli.KAK_END} .* %{ ${KakCli.NOP} %sh{ ${ShellCli.RM} ${tempFile} } }\" ${ShellCli.PIPE} ${KakCli.KAK} ${KakCli.FLAG_P} ${kakClient.client}"
    )
  )

/**
 * shellProcessTemplate is a kotlin.collections.List template for a shell command.
 *
 * `/bin/sh -c $@`
 *
 * @param cliArgs is [CommandLine.cliArgs]
 * @see [org.kjd.shellit.kak.client.CliArgs]
 *
 * @return Commands is a releaseType safe wrapper around [kotlin.collections.List] String
 * @see [org.kjd.shellit.kak.client.Commands]
 */
fun shellProcessTemplate(cliArgs: CliArgs): Commands =
  Commands(
    listOf(
      "${ShellCli.SLASH}${ShellCli.BIN}${ShellCli.SLASH}${ShellCli.SHELL}",
      ShellCli.FLAG_C,
      cliArgs.args.joinToString(" ")
    )
  )
