package org.kjd.shellit.kak.util

/** Timeout returns [Seconds] a kotlin.Long representation format.
 * [Timeout UML](/shellit.kak/src/main/resources/img/Timeout.png) */
sealed class Timeout {
  abstract val Seconds: Long

  object TWENTY : Timeout() {
    override val Seconds: Long = 20L
  }

  object FORTY : Timeout() {
    override val Seconds: Long = 40L
  }

  object SIXTY : Timeout() {
    override val Seconds: Long = 60L
  }
}
