package org.kjd.shellit.kak.closure

import org.kjd.shellit.kak.closure.Result.Failure
import org.kjd.shellit.kak.closure.Result.Success

/**
 * A data structure [Result] that wraps a value or reason in types [Success] or [Failure]
 *
 * [Result UML](/shellit.kak/src/main/resources/img/Result.png)
 *
 * @author Pryce, N & Sanchez, I 2019, 'result.kt', Result4k, viewed on 16 Mar 2020,
 *    https://github.com/npryce/result4k/blob/master/src/main/kotlin/com/natpryce/result.kt
 */
sealed class Result<out A : Any, out T : Throwable> {
  class Success<out A : Any>(val value: A) : Result<A, Nothing>()
  class Failure<out T : Throwable>(val reason: T) : Result<Nothing, T>()
}

/** A closure over a try catch [block] */
inline fun <A : Any> resultFrom(block: () -> A): Result<A, Throwable> =
  try {
    Success(block())
  } catch (e: Throwable) {
    Failure(e)
  }

/** [Result] extension function that pattern matches on its types and executes [block] on [Failure] */
inline fun <A : Any, T : Throwable> Result<A, T>.onFailure(block: (Failure<T>) -> Nothing): A = when (this) {
  is Success<A> -> value
  is Failure<T> -> block(this)
}

/** [Result] extension function that maps [f] to the value */
inline fun <A : Any, B : Any, T : Throwable> Result<A, T>.map(f: (A) -> B): Result<B, T> =
  flatMap { value -> Success(f(value)) }

/** [Result] extension function that takes a value and applies [f] and returns a wrapped value */
inline fun <A : Any, B : Any, T : Throwable> Result<A, T>.flatMap(f: (A) -> Result<B, T>): Result<B, T> =
  when (this) {
    is Success<A> -> f(value)
    is Failure<T> -> this
  }

/** [Result] extension function that takes a reason and applies [f] and returns a wrapped reason */
inline fun <A : Any, T : Throwable, F : Throwable> Result<A, T>.flatMapFailure(f: (T) -> Result<A, F>): Result<A, F> =
  when (this) {
    is Success<A> -> this
    is Failure<T> -> f(reason)
  }

/** [Result] extension function that maps [f] to the reason */
inline fun <A : Any, T : Throwable, U : Throwable> Result<A, T>.mapFailure(f: (T) -> U): Result<A, U> =
  flatMapFailure { reason -> Failure(f(reason)) }

