package org.kjd.shellit.kak.closure

import org.kjd.shellit.kak.closure.Console.isFailure
import org.kjd.shellit.kak.closure.Console.isSuccess
import org.kjd.shellit.kak.closure.Console.outputException
import org.kjd.shellit.kak.closure.Console.print
import org.kjd.shellit.kak.closure.Console.println

/**
 * Console.isSuccess is a function that logs on error and returns a boolean value.
 * It takes a user defined message on failure and wraps a function with no
 * return releaseType inside a try catch block. isSuccess returns true on success
 * and false on failure.
 *
 * [Console UML](/shellit.kak/src/main/resources/img/Console.png)
 *
 * isFailure is the compliment of Console.isSuccess.
 *
 * outputException throws an exception to stdout for ease of debugging a failed process.
 *
 * println and print values implement kotlin.io.println and kotlin.io.print.
 *
 * @author Saumont, P-Y 2019, Fully functional input/output, The Joy of Kotlin,
 *         chpt 12, p.341, ex 12.5, Manning Publications,
 *         https://www.manning.com/books/the-joy-of-kotlin
 *
 * @author Syse, E 2017, KDBC: SQL DSL for Kotlin, kdbc.kt, github.com, viewed on 02 Jan 2020,
 *         https://github.com/edvin/kdbc/blob/master/src/main/kotlin/kdbc/kdbc.kt#L26
 *
 * @author JetBrains 2019, Kotlinx Coroutines, TestUtil.kt, github.com, viewed on 15 Jan 2020,
 *         https://github.com/Kotlin/kotlinx.coroutines/blob/master/kotlinx-coroutines-core/jvm/test/guide/test/TestUtil.kt#L16
 *
 *  @property isSuccess [Console.isSuccess]
 *  @property isFailure [Console.isFailure]
 *  @property outputException [Console.outputException]
 *  @property print [kotlin.io.print]
 *  @property println [kotlin.io.println]
 */
object Console {
  /**
   * isSuccess is a try catch wrapper that returns true on success and logs a failure with return of false.
   *
   * @param msg is the user defined log message
   * @param op the function to try with no return releaseType
   * @return [kotlin.Boolean] true on success and false on failure
   */
  inline fun isSuccess(msg: String, op: () -> Unit): Boolean =
    try {
      op()
      true
    } catch (ex: Throwable) {
      println("$msg\n${ex.cause}\n${ex.message}")
      false
    }

  /**
   * isFailure is a try catch wrapper that returns false on success and logs a failure with return of true.
   *
   * @param msg is the user defined log message
   * @param op the function to try with no return releaseType
   * @return [kotlin.Boolean] true on success and false on failure
   */
  inline fun isFailure(msg: String, op: () -> Unit): Boolean =
    try {
      op()
      false
    } catch (ex: Throwable) {
      println("$msg\n${ex.cause}\n${ex.message}")
      true
    }

  /**
   * outputException throws an exception to stdout for ease of debugging a failed process.
   *
   * @param op the function to try with a generic return releaseType
   * @return R is a generic releaseType of the success
   * @throws [kotlin.printStackTrace] to console
   */
  inline fun <R> outputException(op: () -> R): R =
    try {
      op()
    } catch (ex: Throwable) {
      println(ex.printStackTrace())
      kotlin.system.exitProcess(2)
    }

  val print: (Any) -> Unit = { kotlin.io.print(it.toString()) }

  val println: (Any) -> Unit = { kotlin.io.println(it.toString()) }
}
