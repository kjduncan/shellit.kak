package org.kjd.shellit.kak

import org.kjd.shellit.kak.client.CommandLine
import org.kjd.shellit.kak.closure.Console
import org.kjd.shellit.kak.closure.onFailure
import org.kjd.shellit.kak.continuum.Changes
import org.kjd.shellit.kak.continuum.ContinuumCmd
import org.kjd.shellit.kak.continuum.Description
import org.kjd.shellit.kak.continuum.ReleaseType

/** quit is a terminal call to [kotlin.system.exitProcess] */
private fun quit(code: Int): Nothing {
  kotlin.system.exitProcess(code)
}

/**
 * mainContinuum runs Variant which uses interface composition from
 * ParseCliArgsImpl to ContinuumImpl and ContinuumDT types to pattern
 * match on available commands.
 *
 * A call can be made to four ContinuumImpl interface methods;
 * * addChange,
 * * release,
 * * changeLog,
 * * currentVersion.
 *
 * All Continuum methods return a Result data type which can be
 * either a Success or Failure.
 *
 * `shellit [continuum|cm] [-h] option ...`
 *
 * @param [args] command line arguments prefixed with continuum or cm
 * @return [Console.println] [org.kjd.shellit.kak.closure.onFailure]
 * @return [Console.println] [org.kjd.shellit.kak.closure.resultFrom]
 * @return [Usage.MESSAGE_CONTINUUM] long help message to the user
 * @return [Usage.MESSAGE_CONTINUUM_SHORT] short help message to the user
 * @return [kotlin.Unit]
 *
 * @throws [quit] on invalid arguments
 */
private fun mainContinuum(args: Array<String>) {
  require(args.size > 1)
  {
    Console.println("\nERROR: continuum requires a minimum of 2 positional arguments != ${args.size}\n${Usage.MESSAGE_CONTINUUM_SHORT}")
    quit(2)
  }

  val variant = object : Variant {}

  variant.run {
    continuumCliArgs()(args).let { cmd: ContinuumCmd ->
      when (cmd) {
        ContinuumCmd.add -> {
          require(args.size == 4)
          {
            Console.println("\nERROR: continuum add requires 4 positional arguments != ${args.size}\nUsage: $ continuum add major|minor|patch \"a description in double quotes\"")
            quit(2)
          }

          addChange(Changes(
              ReleaseType.valueOf(args[2].toLowerCase()),
              Description(args[3])))
            .onFailure { return Console.println(it.reason.message.toString()) }
        }

        ContinuumCmd.changelog -> {
          val log = changeLog()
            .onFailure { return Console.println(it.reason.message.toString()) }
          Console.println(log)
        }

        ContinuumCmd.current -> {
          val current = currentVersion()
            .onFailure { return Console.println(it.reason.message.toString()) }
          Console.println(current)
        }

        ContinuumCmd.release -> {
          release()
            .onFailure { return Console.println(it.reason.message.toString()) }
        }

        ContinuumCmd.error -> Console.println(Usage.MESSAGE_CONTINUUM)
      }
    }
  }
}

/**
 * mainDebug runs Debug which uses function composition via chained method calls
 * left to right within the Reader monad closure.
 *
 * A call to ParseCliArgsImpl interface method kakouneCliArgs with the parsed
 * arguments passed to the extension function `T.let` which provides the closure
 * for the CommandLine data class.
 *
 * ProcessBuilderDebugImpl interfaces method processBuilderOnDebug, actions the
 * closure which ends the call stack in a termination function Reader.runReader.
 *
 * mainDebug is for terminal/console invocation as its output prints to console.
 *
 * `shellit.jar -d [binary|jar|script] ...`
 *
 * `shellit.run -d [binary|jar|script] ...`
 *
 * @param [args] command line arguments with the `-d` flag.
 * @return [kotlin.Unit]
 */
private fun mainDebug(args: Array<String>): Unit {

  val debugit = object : Debug {}

  debugit.run {
    kakouneCliArgs()(args).let { commandLine: CommandLine ->
      processBuilderOnDebug(commandLine)
        .runReader(commandLine)
    }
  }
  Console.println("Application has completed...")
  Console.println("If preamble is absent the binary/jar/script/other is not on your \$PATH.\n\n")
}

/**
 * mainIgnore runs Shell which uses function composition via chained method calls
 * left to right within the Reader monad closure.
 *
 * A call to the ProcessBuilderShellImpl interface method processBuilderIgnoreOutput
 * with the preparsed arguments from ParseCliArgsImpl interface method kakouneCliFlagArgs.
 *
 * Reader.map then pulls the CommandLine data class out of the Reader closure and passes
 * it to the ProcessBuilderShellImpl interfaces method processBuilderOnSuccess,
 * which then ends the call stack in a termination function Reader.runReader.
 *
 * `shellit -i [binary|jar|script] ...`
 *
 * @param [args] command line arguments with the `-i` flag from Kakoune client.
 * @return [kotlin.Unit]
 */
private fun mainIgnore(args: Array<String>): Unit {

  val shell: Shell = object : Shell {}

  shell.run {
    processBuilderIgnoreOutput(kakouneCliFlagArgs()(args))
      .map { commandLine: CommandLine ->
        processBuilderOnSuccess(commandLine)
          .runReader(commandLine)
      }
      .runReader(this)
  }
}

/**
 * mainProcess runs Shell which uses function composition via chained method calls
 * left to right within the Reader monad closure.
 *
 * A call to the ProcessBuilderShellImpl interface method processBuilderRedirectOutput
 * with the preparsed arguments from ParseCliArgsImpl interface method kakouneCliArgs.
 *
 * Reader.map then pulls the CommandLine data class out of the Reader closure and passes
 * it to the ProcessBuilderShellImpl interfaces method processBuilderConnectToKakProcess
 * which then ends the call stack in a termination function Reader.runReader.
 *
 * `shellit [binary|jar|script] ...`
 *
 * @param [args] command line arguments with no flags from Kakoune client.
 * @return [kotlin.Unit]
 */
private fun mainProcess(args: Array<String>): Unit {

  val shell: Shell = object : Shell {}

  shell.run {
    processBuilderRedirectOutput(kakouneCliArgs()(args))
      .map { commandLine: CommandLine ->
        processBuilderConnectToKakProcess(commandLine)
          .runReader(commandLine)
      }
      .runReader(this)
  }
}

/**
 * The main methods position 0 args is set to the $kak_session from the invoking Kakoune client.
 *
 * [Shell-Debug-Variant-Object UML](/shellit.kak/src/main/resources/img/ShellDebugVariantObj.png)
 *
 * [Shell UML](/shellit.kak/src/main/resources/img/Shell.png)
 * [Debug UML](/shellit.kak/src/main/resources/img/Debug.png)
 * [Variant UML](/shellit.kak/src/main/resources/img/Variant.png)
 *
 * [ShellitKt UML](/shellit.kak/src/main/resources/img/ShellitKt.png)
 *
 * * Given what I have, is it `possible` to get what I want?
 * * Given what I have, what is the `minimum cost` to get what I want?
 * * Given what I have, what is the `set of ways` to get what I want?
 *
 * @author Fong, B & Spivak, D 2018, ‘Seven Sketches in Compositionality:
 *     An Invitation to Applied Category Theory’ Chapter 2, p.39, Cornell University,
 *     arXiv.org, viewed on 14 Jan 2020, https://arxiv.org/abs/1803.05316
 *
 * @author Oracle 2019, Secure Coding Guidelines for Java SE,
 *     Updated for Java SE 13, Document version: 7.3, viewed on 27 Nov 2019,
 *     https://www.oracle.com/technetwork/java/seccodeguide-139067.html
 *
 * @author The Open Group 2018, 'sh - shell, the standard command language interpreter',
 *     Shell & Utilities, The Open Group Base Specifications Issue 7,
 *     2018 edition IEEE Std 1003.1-2017 (Revision of IEEE Std 1003.1-2008),
 *     viewed 14 Feb 2020, https://pubs.opengroup.org/onlinepubs/9699919799/
 *
 * @return [mainContinuum] allows you to manage semantic versioning and changelog generation files.
 * @see [org.kjd.shellit.kak.mainContinuum]
 *
 * @return [mainDebug] a process from the terminal `shellit.* -d [binary|jar|script]...`
 * @see [org.kjd.shellit.kak.mainDebug]
 *
 * @return [mainIgnore] processes output from the kakoune client
 * @see [org.kjd.shellit.kak.mainIgnore]
 *
 * @return [mainProcess] runs a process and opens its stdout in a temp file within kakoune client
 * @see [org.kjd.shellit.kak.mainProcess]
 *
 * @return [Usage.MESSAGE_LONG] prints long usage instructions to console
 * @see [org.kjd.shellit.kak.Usage]
 *
 * @return [Usage.MESSAGE_SHORT] prints short usage instructions to console
 * @see [org.kjd.shellit.kak.Usage]
 *
 * @throws [quit] a terminal call to [kotlin.system.exitProcess]
 */
fun main(args: Array<String>) {
  require(args.isNotEmpty() && args.size + 1 != Int.MIN_VALUE)
  { Console.println(Usage.MESSAGE_LONG); quit(2) }

  val clone: Array<String> = args.clone()

  val isDebug: String.() -> Boolean      = { startsWith("-d") }
  val isIgnore: String.() -> Boolean     = { startsWith("-i") }
  val isLongHelp: String.() -> Boolean   = { startsWith("--h") }
  val isShortHelp: String.() -> Boolean  = { startsWith("-h") }
  val isContinuum: String.() -> Boolean  = { contentEquals("cm") ||
                                             contentEquals("continuum") }
  val isSimpleFlag: String.() -> Boolean = { startsWith("-") }

  when {
    clone[0].isLongHelp()   -> Console.println(Usage.MESSAGE_LONG)
    clone[0].isShortHelp()  -> Console.println(Usage.MESSAGE_SHORT)
    clone[0].isContinuum()  -> mainContinuum(clone)
    clone.size < 2          -> Console.println(Usage.MESSAGE_SHORT)
    clone[0].isDebug()      -> mainDebug(clone)
    clone[1].isIgnore()     -> mainIgnore(clone)
    clone[1].isSimpleFlag() -> Console.println(Usage.MESSAGE_SHORT)
    else                    -> mainProcess(clone)
  }
  quit(0)
}
