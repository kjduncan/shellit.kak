package org.kjd.shellit.kak.io

import org.kjd.shellit.kak.client.CommandLine
import org.kjd.shellit.kak.client.Message
import org.kjd.shellit.kak.client.OnFailure
import org.kjd.shellit.kak.client.OnSuccess
import org.kjd.shellit.kak.closure.Reader
import org.kjd.shellit.kak.util.Timeout.TWENTY
import java.util.concurrent.TimeUnit

/**
 * ProcessBuilderShell interface a framework around the java.lang.Process.
 * @see [ProcessBuilderShellImpl] for method implementation
 */
interface ProcessBuilderShell<ENV : Any> {
  fun ENV.processBuilderCommands(
    commands: List<String>,
    processBuilder: ProcessBuilder
  ): ProcessBuilder

  fun ENV.processBuilderConnectToKakProcess(commandLine: CommandLine)
    : Reader<CommandLine, Boolean>

  fun ENV.processBuilderRedirectOutput(commandLine: CommandLine)
    : Reader<ENV, CommandLine>

  fun ENV.processBuilderIgnoreOutput(commandLine: CommandLine)
    : Reader<ENV, CommandLine>

  fun ENV.processBuilderOnError(commandLine: CommandLine)
    : Reader<CommandLine, Boolean>

  fun ENV.processBuilderOnSuccess(commandLine: CommandLine)
    : Reader<CommandLine, Boolean>
}

/** ProcessBuilderShellImpl implements [ProcessBuilderShell]
 * [ProcessBuilderShell UML](/shellit.kak/src/main/resources/img/ProcessBuilderShell.png) */
interface ProcessBuilderShellImpl<ENV : Any> : ProcessBuilderShell<ENV> {
  /**
   * processBuilderCommands helper method for [java.lang.ProcessBuilder].
   *
   * @param commands [kotlin.collections.List]<String>
   * @param processBuilder [java.lang.ProcessBuilder]
   * @return [java.lang.ProcessBuilder.command]
   */
  override fun ENV.processBuilderCommands(
    commands: List<String>,
    processBuilder: ProcessBuilder
  ): ProcessBuilder {
    return processBuilder.command(commands)
  }

  /**
   * processBuilderOnSuccess is a Reader<CommandLine, Boolean> that
   * provides the closure for [kotlin.runCatching] a [java.lang.Process].
   *
   * @param commandLine [CommandLine]
   * @return [Reader]<[CommandLine], [Boolean]>
   * @return [Boolean] true on success and false on failure
   */
  override fun ENV.processBuilderOnSuccess(commandLine: CommandLine)
    : Reader<CommandLine, Boolean> =
    Reader {
      kotlin.runCatching {
        commandLine.message = Message(
          markup = OnSuccess.markup(
            commandLine.cliArgs.args.joinToString()
          ).toString()
        )

        processBuilderCommands(
          commands = commandLine.kakMessageMarkup.value.cmds,
          processBuilder = ProcessBuilder()
        ).start()
          .also { process: Process ->
            process.waitFor(TWENTY.Seconds, TimeUnit.SECONDS)
          }.destroy()
      }.isSuccess
    }

  /**
   * processBuilderOnError is a Reader<CommandLine, Boolean> that
   * provides the closure for [kotlin.runCatching] a [java.lang.Process].
   *
   * `echo -markup '{Error}Onload message WENT BAD'" | kak -p client`
   *
   * @param commandLine [CommandLine]
   * @return [Reader]<[CommandLine], [Boolean]>
   * @return [Boolean] true on success and false on failure
   */
  override fun ENV.processBuilderOnError(commandLine: CommandLine)
    : Reader<CommandLine, Boolean> =
    Reader {
      kotlin.runCatching {
        processBuilderCommands(
          commands = commandLine.kakMessageMarkup.value.cmds,
          processBuilder = ProcessBuilder()
        ).start()
          .also { process: Process ->
            process.waitFor(TWENTY.Seconds, TimeUnit.SECONDS)
          }.destroy()
      }.isSuccess
    }

  /**
   * processBuilderConnectToKakProcess is a Reader<CommandLine, Boolean>
   * that provides the closure for [kotlin.runCatching] a [java.lang.Process].
   *
   * @param commandLine [CommandLine]
   * @return [Reader]<[CommandLine], [Boolean]>
   * @return [Boolean] true on success and false on failure
   */
  override fun ENV.processBuilderConnectToKakProcess(commandLine: CommandLine)
    : Reader<CommandLine, Boolean> =
    Reader {
      kotlin.runCatching {
        processBuilderCommands(
          commands = commandLine.kakConnectProcess.value.cmds,
          processBuilder = ProcessBuilder()
        ).start()
          .also { process: Process ->
            process.waitFor(TWENTY.Seconds, TimeUnit.SECONDS)
          }.destroy()
      }.isSuccess
    }

  /**
   * processBuilderRedirectOutput is a Reader<ENV, CommandLine>
   * that provides the closure for a [java.lang.Process].
   *
   * `shellit [binary|jar|script] ... > tempfile 2>&1`
   *
   * @param commandLine [CommandLine]
   * @return [Reader] that wraps a [CommandLine] in closure
   * @return [processBuilderOnError]
   */
  override fun ENV.processBuilderRedirectOutput(commandLine: CommandLine)
    : Reader<ENV, CommandLine> =
    Reader {
      processBuilderCommands(
        commands = commandLine.runShellProcess.value.cmds,
        processBuilder = ProcessBuilder()
      ).redirectOutput(commandLine.lazyFile.value)
        .redirectError(commandLine.lazyFile.value)
        .start()
        .also { process: Process ->
          process.waitFor(TWENTY.Seconds, TimeUnit.SECONDS)
        }.destroy()

      commandLine
    }

  /**
   * processBuilderIgnoreOutput is a Reader<ENV, CommandLine>
   * that provides the closure for [kotlin.runCatching] a [java.lang.Process].
   *
   * `shellit -i [binary|jar|script] ...`
   *
   * @param commandLine [CommandLine]
   * @return [Reader] that wraps a [CommandLine] in closure
   * @return [processBuilderOnError]
   */
  override fun ENV.processBuilderIgnoreOutput(commandLine: CommandLine)
    : Reader<ENV, CommandLine> =
    Reader {
      kotlin.runCatching {
        processBuilderCommands(
          commands = commandLine.runShellProcess.value.cmds,
          processBuilder = ProcessBuilder()
        ).start()
          .also { process: Process ->
            process.waitFor(TWENTY.Seconds, TimeUnit.SECONDS)
          }.destroy()

        commandLine
      }.getOrElse {
        commandLine.message = Message(
          markup = OnFailure.markup(
            it.message.toString()
          ).toString()
        )
        processBuilderOnError(commandLine)
          .runReader(commandLine)
        commandLine
      }
    }
}
