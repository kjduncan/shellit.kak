package org.kjd.shellit.kak.io

import org.kjd.shellit.kak.client.CommandLine
import org.kjd.shellit.kak.closure.Console
import org.kjd.shellit.kak.closure.Reader
import org.kjd.shellit.kak.util.DurationTimer
import org.kjd.shellit.kak.util.Timeout
import java.time.ZoneId
import java.util.concurrent.TimeUnit

/**
 * ProcessBuilderDebug interface a wrapper around [java.lang.Process].
 * @see [ProcessBuilderDebugImpl] for method implementation
 */
interface ProcessBuilderDebug<in ENV : Any> {
  fun ENV.processBuilderDebugCommands(
    commands: List<String>,
    processBuilder: ProcessBuilder
  ): ProcessBuilder

  fun ENV.processBuilderOnDebug(commandLine: CommandLine)
    : Reader<CommandLine, Unit>
}

/** [ProcessBuilderDebugImpl] implements [ProcessBuilderDebug]
 * [ProcessBuilderDebug UML](/shellit.kak/src/main/resources/img/ProcessBuilderDebug.png) */
interface ProcessBuilderDebugImpl<in ENV : Any> : ProcessBuilderDebug<ENV> {
  /**
   * processBuilderDebugCommands helper method for java.lang.ProcessBuilder.
   *
   * @param commands [kotlin.collections.List]
   * @param processBuilder [java.lang.ProcessBuilder]
   * @return [java.lang.ProcessBuilder.command]
   */
  override fun ENV.processBuilderDebugCommands(
    commands: List<String>,
    processBuilder: ProcessBuilder
  ): ProcessBuilder {
    return processBuilder.command(commands)
  }

  /**
   * processBuilderOnDebug is a Reader<CommandLine, Boolean> that
   * provides the closure for kotlin.runCatching a java.lang.Process.
   * The Console.println displays java.lang.ProcessHandle.info to console.
   *
   * The purpose of this method is to record the applications transactions,
   * and report to the terminal via [Console].
   *
   * `shellit.jar -d [binary|jar|script]...`
   *
   * `shellit.run -d [binary|jar|script]...`
   *
   * @author Oracle 2019, Getting Information About a Process, Process API,
   *    Core Libraries, Java 11, viewed 26 Nov 2019,
   *    https://docs.oracle.com/en/java/javase/11/core/process-api1.html#GUID-30EA2C53-C6BA-422F-9F04-2660525B7F3D
   *
   * @param commandLine
   * @see org.kjd.shellit.kak.client.CommandLine
   * @return [Reader]<[CommandLine], [Unit]>
   * @see org.kjd.shellit.kak.closure.Reader
   * @throws [Console.outputException] on process failure
   */
  override fun ENV.processBuilderOnDebug(commandLine: CommandLine)
    : Reader<CommandLine, Unit> =
    Reader {
      Console.outputException {
        DurationTimer.start()

        val processBuilder: ProcessBuilder = processBuilderDebugCommands(
          commands = commandLine.runShellProcess.value.cmds,
          processBuilder = ProcessBuilder()
        )
        Console.println("\n")
        Console.println(
          "ProcessBuilder.commands(${processBuilder.command().joinToString()})"
        )

        val process: Process by lazy { processBuilder.start() }

        val na = "<Process.isAlive="
        val template: String = """
            Process ID: ${process.pid()}

            Command name: ${process.info().command().orElse("$na${process.isAlive}>")}
            Command line: ${process.info().commandLine().orElse("$na${process.isAlive}>")}
            Start time: ${process.info().startInstant().map {
          it.atZone(ZoneId.systemDefault()).toLocalDateTime().toString()
        }.orElse("$na${process.isAlive}>")}
            User: ${process.info().user().orElse("$na${process.isAlive}>")}

            """.trimIndent()

        Console.println(template)
        process.waitFor(Timeout.TWENTY.Seconds, TimeUnit.SECONDS)
          .also { finished: Boolean ->
            /* Has the process terminated before the end of wait time, negate true. */
            if (!finished) process.destroy()
          }
        Console.println(
          "Is java.lang.Process still alive: ${process.isAlive}\n\n If Process ID was not null and Command name: <Process.isAlive=false>, the java.lang.Process completed before java.lang.ProcessHandle.Info could be utilised.\n"
        )
        DurationTimer.stop()
      }
    }
}
