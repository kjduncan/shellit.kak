package org.kjd.shellit.kak.continuum

/** Continuum available user commands */
enum class ContinuumCmd { add, changelog, current, release, error; }

/** Continuum command [ContinuumCmd.add] release types */
enum class ReleaseType { major, minor, patch, error; }

/** Type safe [Changes.description] field */
inline class Description(val value: String)

/** Continuum Changes takes a [ReleaseType], [Description] and overrides [toString] to represent a map / json
 * [ContinuumDT UML](/shellit.kak/src/main/resources/img/ContinuumDT.png) */
data class Changes(val type: ReleaseType, val description: Description) {
  override fun toString(): String = """{"type": "${type.name}", "description": "${description.value}"}"""
}

/** Continuum exception implements [kotlin.Throwable] */
class ContinuumException(override val message: String) : Throwable(message)

