#!/bin/sh
# `.jar` suffix is no longer required from bash command line
exec java -jar "$JARPATH/$1.jar" "${@:2}"
exit 1
