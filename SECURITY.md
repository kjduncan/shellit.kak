This file will document secure programming practices for jvm/shellit.kak user implementation.  
  
`java -Djava.security.manager -Djava.security.policy==shellit.policy ...`  
  
```
keystore "/<path-to-your>/publicKey.store";
keystorePasswordURL "/<path-to-your>/publicpwd";

grant signedBy "kjduncan", codeBase "file:./shellit.kak.jar" {
  permission java.lang.RuntimePermission "blah", "read";
  permission java.io.FilePermission "/bin/sh", "execute";
  permission java.io.FilePermission "${user.home}/blah/tmp/kak-shellit-*", "read,write,delete";
  ...;
};
```
  
`keytool` the Key and Certificate Management Tool  
`jarsigner` sign and verify Java Archive (JAR) files  
`jlink` assemble and optimize a set of modules and their dependencies into a custom runtime image  
  
KJ_Duncan, work in progress, https://bitbucket.org/KJ_Duncan/jse-security-guide/src/master/  
  
Oracle 2020, Java Platform, Standard Edition, Security Developer's Guide, Release 14, viewed on 12 May 2020, https://docs.oracle.com/en/java/javase/14/security/java-security-overview1.html  
  
Oracle 2020, The keytool Command, Java Standard Edition, Release 14, viewed on 12 May 2020, https://docs.oracle.com/en/java/javase/14/docs/specs/man/keytool.html  
  
Oracle 2020, The jarsigner Command, Java Standard Edition, Release 14, viewed on 12 May 2020, https://docs.oracle.com/en/java/javase/14/docs/specs/man/jarsigner.html  
  
Oracle 2020, Java Platform, Standard Edition, Security Developer's Guide, Release 14, viewed on 12 May 2020, https://docs.oracle.com/en/java/javase/14/docs/specs/man/jlink.html  
  
