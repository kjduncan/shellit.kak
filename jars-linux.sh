#!/bin/sh

##
# Tommaso, P 2019, How to make a JAR file Linux executable, corderwall.com,
# viewed on 6 Jan 2019, <https://coderwall.com/p/ssuaxa/how-to-make-a-jar-file-linux-executable>
##

MYSELF=`which "$0" 2>/dev/null`

[ $? -gt 0 -a -f "$0" ] && MYSELF="./$0"

java=java

if test -n "$JAVA_HOME"; then
    java="$JAVA_HOME/bin/java"
fi

exec "$java" $java_args -jar $MYSELF "$@"

exit 1

# cat jars-linux.sh shellit.kak-<version>.jar > shellit.run && chmod u+x,u-w,og-rwx shellit.run && mv shellit.run ~/.local/bin/
