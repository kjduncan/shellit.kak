#### Working with the Command Line Compiler as at 08/04/2020 ####


Where compiler.options and classes are files in the `pwd` directory which holds the projects *.kt files.  
[Kotlin Compiler Options](https://kotlinlang.org/docs/reference/compiler-reference.html#compiler-options).  
  
```shell

$ kotlinc @compiler.options @classes
$ ls
shellit.kak-<version>.jar

```
---

```
compiler.options
-include-runtime -jvm-target 11 -no-reflect -Xallow-no-source-files -Xinline-classes -d shellit.kak-<version>.jar

classes
Shellit.kt Scope.kt ParseCliArgs.kt Usage.kt CommandDT.kt CommandTemplates.kt ExecCli.kt MessageClient.kt Console.kt Reader.kt Result.kt Continuum.kt ContinuumDT.kt ProcessBuilderDebug.kt ProcessBuilderShell.kt DurationTimer.kt Timeout.kt
```

