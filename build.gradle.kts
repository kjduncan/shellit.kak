plugins {
  `maven-publish`
  kotlin("jvm") version "1.4.31"
  id("org.jetbrains.dokka") version "1.4.20"
}

group = "org.kjd.shellit.kak"
version = "3.1"

repositories {
  mavenCentral()
  jcenter()
}

dependencies {
  constraints {
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")

    testImplementation("org.assertj:assertj-core:3.19.0")
    testImplementation("io.mockk:mockk:1.10.6")
  }

  dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.junit.jupiter:junit-jupiter-api")

    testImplementation("org.assertj:assertj-core")
    testImplementation("io.mockk:mockk")
  }
}

// "standard_out",
// "standard_error"

tasks {
  test {
    useJUnitPlatform()
    jvmArgs = listOf("-ea")
    failFast = true
    testLogging.showStandardStreams = true
    testLogging {
      events(
        "passed",
        "failed",
        "skipped"
      )
    }
  }

  jar {
    enabled = true
    manifest {
      attributes(
        "Manifest-Version" to "1.0",
        "Created-By" to "kjd016 (Kirk Duncan)",
        "Name" to "org/kjd/shellit/kak/",
        "Sealed" to "true",
        "Application-Name" to "shellit.kak",
        "Main-Class" to "org.kjd.shellit.kak.ShellitKt",
        "Implementation-Title" to "Shellit a Kakoune command line gateway",
        "Implementation-Version" to project.version
      )
    }
  }

  compileKotlin {
    kotlinOptions.jvmTarget = "15"
    kotlinOptions.freeCompilerArgs = listOf(
      "-no-stdlib",
      "-no-reflect",
      "-Xallow-no-source-files",
      "-Xinline-classes"
    )
  }

  compileTestKotlin { kotlinOptions.jvmTarget = "15" }
}

/* Dokka 2020, Using the Gradle plugin, v1.4.20, Jetbrains, viewed 02 March 2021,
 *             https://kotlin.github.io/dokka/1.4.20/user_guide/gradle/usage/
 */
// val dokkaJar: Jar by tasks.creating(Jar::class) { }

val uberJar: Jar by tasks.creating(Jar::class) {
  archiveClassifier.set("uber")

  manifest {
    attributes(
      "Manifest-Version" to "1.0",
      "Created-By" to "kjd016 (Kirk Duncan)",
      "Name" to "org/kjd/shellit/kak/",
      "Sealed" to "true",
      "Application-Name" to "shellit.kak",
      "Main-Class" to "org.kjd.shellit.kak.ShellitKt",
      "Implementation-Title" to "Shellit a Kakoune command line gateway",
      "Implementation-Version" to project.version
    )
  }

  from(sourceSets.main.get().output)
  exclude { details: FileTreeElement ->
    details.file.name.endsWith(".png") ||
    details.file.name.endsWith(".puml")
  }

  dependsOn(configurations.runtimeClasspath)
  from({ configurations.runtimeClasspath.get().filter {
    it.name.endsWith(".jar")}.map { zipTree(it) }})
}

publishing {
  publications {
    create<MavenPublication>("default") {
      // artifact(dokkaJar)
      artifact(uberJar)
    }
  }

  repositories {
    maven { url = uri("$buildDir/repository") }
  }
}
